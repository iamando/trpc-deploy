# tRPC Frontend Deploy

## Build

Run this command to build app for production

```bash
pnpm run build
```

## Scripts

Running on development

```bash
pnpm run start:dev
```

Running on production

```bash
pnpm run start:prod
```

## Support

tRPC Frontend Deploy is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.

## License

tRPC Frontend Deploy is [MIT licensed](../LICENSE).
