export {}

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production' | 'test'
      PORT: string
      REACT_APP_TRPC_BACKEND_URL: string
    }
  }
}
