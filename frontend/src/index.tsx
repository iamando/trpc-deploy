import React, { StrictMode, Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import { QueryClientProvider } from '@tanstack/react-query'

import './index.css'

import { TRPCProvider, trpcClient, queryClient } from './utils/trpc'

import Home from './pages/Home'

import Loader from './components/Loader'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
  <TRPCProvider client={trpcClient} queryClient={queryClient()}>
    <QueryClientProvider client={queryClient()}>
      <StrictMode>
        <Suspense fallback={<Loader />}>
          <Home />
        </Suspense>
      </StrictMode>
    </QueryClientProvider>
  </TRPCProvider>
)
