import { createTRPCReact, httpBatchLink } from '@trpc/react-query'
import { QueryClient } from '@tanstack/react-query'
import type { BackendTRPCRouter } from 'backend/src/types/trpc'

const trpc = createTRPCReact<BackendTRPCRouter>()

export const queryClient = () =>
  new QueryClient({ defaultOptions: { queries: { staleTime: 5 * 1000 } } })

export const trpcClient = trpc.createClient({
  links: [
    httpBatchLink({
      url: process.env.REACT_APP_TRPC_BACKEND_URL,
    }),
  ],
})

export const TRPCProvider = trpc.Provider
export const trpcProcedure = trpc
