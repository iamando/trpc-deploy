import React from 'react'

import { trpcProcedure } from '../utils/trpc'

const Home = () => {
  const healthcheck = trpcProcedure.healthcheck.useQuery()

  return (
    <div className="Home">
      <h1>{healthcheck.data?.message}</h1>
    </div>
  )
}

export default Home
