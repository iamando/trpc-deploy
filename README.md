# tRPC Deploy

## Build

Build backend and frontend app using this command in root directory

```bash
pnpm run build
```

## Scripts

Run all repos concurrently

```bash
pnpm run start
```

Or use these scripts to run directly each app on root directory

### Backend

Running on development

```bash
pnpm run start:backend:dev
```

Running on production

```bash
pnpm run start:backend:prod
```

### Frontend

Running on development

```bash
pnpm run start:frontend:dev
```

Running on production

```bash
pnpm run start:frontend:prod
```

## Support

tRPC Deploy is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.

## License

tRPC Deploy is [MIT licensed](LICENSE).
