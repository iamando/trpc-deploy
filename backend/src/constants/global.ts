import dotenv from 'dotenv'
import path from 'path'

export const BASE_DIR = `${process.cwd()}/backend`

dotenv.config({ path: path.join(BASE_DIR, '.env') })

export const PORT = process.env.PORT
export const REDIS_URL = process.env.REDIS_URL
export const MONGO_URL = process.env.MONGO_URL
