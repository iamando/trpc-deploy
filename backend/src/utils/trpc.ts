import { initTRPC, inferAsyncReturnType } from '@trpc/server'
import * as trpcAdaptersExpress from '@trpc/server/adapters/express'

type Context = inferAsyncReturnType<typeof createContext>

const t = initTRPC.context<Context>().create()

export const createContext = ({
  req,
  res,
}: trpcAdaptersExpress.CreateExpressContextOptions) => ({ req, res })
export const router = t.router
export const procedure = t.procedure
export const trpcExpress = trpcAdaptersExpress
