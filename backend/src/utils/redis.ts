import { createClient, RedisClientType } from 'redis'

import { REDIS_URL } from '../constants/global'

const client: RedisClientType = createClient({ url: REDIS_URL })

export const redis = {
  client,
  connect: () => {
    client
      .connect()
      .then(() => {
        console.log('📦 Redis client connected!')
      })
      .catch(err => {
        console.log('An error occured on connecting redis client', err)
        process.exit(1)
      })
  },
}
