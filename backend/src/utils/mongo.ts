import { createConnection } from 'mongoose'

import { MONGO_URL } from '../constants/global'

const client = createConnection(MONGO_URL)

const setOpts = async () => {
  await client.set('useNewUrlParser', true)
  await client.set('useFindAndModify', false)
  await client.set('useCreateIndex', true)
  await client.set('useUnifiedTopology', true)
}

export const mongo = {
  client,
  connect: async () => {
    await setOpts()

    client
      .asPromise()
      .then(() => {
        console.log('🍃 MongoDB connected!')
      })
      .catch(err => {
        console.log('An error occured on connecting mongo database', err)
        process.exit(1)
      })
  },
}
