import router from '../router'

export type BackendTRPCRouter = typeof router
