export {}

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production' | 'test'
      PORT: string
      MONGO_URL: string
      REDIS_URL: string
    }
  }
}
