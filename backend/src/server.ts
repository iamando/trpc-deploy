import 'dotenv/config'
import express, { Express } from 'express'
import morgan from 'morgan'
import cors from 'cors'

import { trpcExpress, createContext } from './utils/trpc'
import { redis } from './utils/redis'
import { mongo } from './utils/mongo'

import { PORT } from './constants/global'

import router from './router'

const app: Express = express()

if (process.env.NODE_ENV !== 'production') app.use(morgan('dev'))

app.use(cors({ credentials: true }))
app.use(express.json())
app.use(
  '/api/trpc',
  trpcExpress.createExpressMiddleware({ router, createContext })
)

app.listen(PORT, () => {
  console.log(`🚀 Server listening on port ${PORT}`)

  mongo.connect()
  redis.connect()
})
