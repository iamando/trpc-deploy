import { router as trpcRouter, procedure } from './utils/trpc'

const router = trpcRouter({
  healthcheck: procedure.query(async () => {
    return {
      message: 'Server is running',
    }
  }),
})

export default router
